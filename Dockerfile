# our final base image
FROM openjdk:8u171-jre-alpine

# set deployment directory
WORKDIR /hw-mvn

# copy over the built artifact from the maven image
COPY --from=maven target/hw-mvn-*.jar ./

# set the startup command to run your binary
CMD ["java", "-jar", "./target/hw-mvn.jar"]
